﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TachoService
{
    
    class History
    {
        public Guid Id;

        public String Timestamp;

        public String Number;

        public String Files;

        public String IMEI;

        public String TachoTimeFrom;

        public String TachoTimeTo;

        public String DataBlocks;
    }
}
