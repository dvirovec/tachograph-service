﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace TachoService
{

    //http://ftp.t-matix.com:9999/tachoservice/m2msms?api_id=302566&number_id=29334&to_number=436768502022&from=4367619361193&text=ERROR%3A7&time=14022017143513
    //http://54.229.87.167:8008/vehicledata/sendOperationalMachineData

    [ServiceContract(Namespace = "http://TachoService.SmsErrorHandling")]
    public interface ISmsErrorHandling
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            UriTemplate = "imei_phone?imei={imei}&phone={phone}")]
        string sendIMEIPhone(string IMEI, string phone);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            UriTemplate = "m2msms?api_id={api_id}&number_id={number_id}&to_number={to_number}&from={from}&time={time}&text={text}")]
        string tmobileSms(string api_id, string number_id, string to_number, string from, string time, string text);
                

    }
   
    public class SmsErrorHandling : ISmsErrorHandling
    {


        private string parseMessage(string from_number, string time, string text) {

            string response_message, message;
            Boolean is_error=true;

            if (!text.Contains("ERROR"))
            {
                is_error = false;
                message = "Request received. Data file shell be downloaded on the ftp server location.";
            }
            else if (text.Substring(6, 1) == "1")
            {
                message = "Internal message error: too many data fields provided, should be 6.";
            }
            else if (text.Substring(6, 1) == "2")
            {
                message = String.Format("Internal message error: invalid data length for field {0}", text.Length > 8 ? text.Substring(8) : "");
            }
            else if (text.Substring(6, 1) == "3")
            {
                message = String.Format("Internal message error: not all data fields provided, missing {0}", text.Length > 8 ? text.Substring(8) : "");
            }
            else if (text.Substring(6, 1) == "4")
            {
                message = "Internal TCU error: buffer overflow";
            }
            else if (text.Substring(6, 1) == "5")
            {
                message = "Message error: wrong IMEI provided";
            }
            else if (text.Substring(6, 1) == "6")
            {
                message = String.Format("TCU error: hardware fault, TCU error code: {0}", text.Length > 8 ? text.Substring(8) : "");
            }
            else if (text.Substring(6, 1) == "7")
            {
                message = "Ignition off, no data download possible.";
            }
            else
            {
                message = String.Format("Unknown error: {0}", text.Length > 8 ? text.Substring(8) : "");
            }
            
            
            response_message = @"{""" + from_number + @""":{""version"":""0.1"",""time"":""" + parseTime(time) +
                                            @""",""raw_text"":""SMS received"",""is_error"":"+(is_error ? "true" : "false")+@",""text"":""" + message + @"""}}";

            return response_message;
        }

        public String tmobileSms(string api_id, string number_id, string to_number, string from_number, string time, string text) {

            Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information,
                String.Format(this.GetType().Name + ":Message received - API_ID:{0}\nID:{1}\nTo:{2}\nFrom:{3}\nTime:{4}\nText:{5}", 
                                api_id, number_id, to_number, from_number, time, text));
                
            if (api_id != Config.API_ID) {                
                
                OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
                response.StatusCode = HttpStatusCode.BadRequest;
                response.StatusDescription = "You are not allowed to access this service.";

                return response.ToString();

                }

            string response_message = parseMessage(from_number, time, text);            
           
            TcpClient client = null;

            try
            {               
                client = new TcpClient(Config.TMATIX_IP, Config.TMATIX_PORT);
                
                 Byte[] data = System.Text.Encoding.ASCII.GetBytes(response_message);

                using (NetworkStream stream = client.GetStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, this.GetType().Name + ":Message forwarded.\n=>" + response_message);

            }
            catch (Exception e)
            {
                System.Console.WriteLine(e.Message);
            }
            finally
            {                
                if (client != null) client.Close();
                
            }

            return response_message;

        }

        private string parseTime(string time)
        {
            try
            {                               
                //DateTime date = DateTime.ParseExact(time, "ddMMyyyyHHmmss", null);

                DateTime date = DateTime.UtcNow;

                string sDateTime = date.ToString("yyyy-MM-dd HH:mm:ss");
               
                return sDateTime;
            }
            catch (Exception e)
            {
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Error, this.GetType().Name+"->"+time+" "+e.Message);
            }

            return "Error time format";
        }

        public string sendIMEIPhone(string IMEI, string phone_number)
        {
            if (phone_number.Substring(0, 1) == "+")
            {
                phone_number = phone_number.Substring(1).Trim();
            }
            else if (phone_number.Substring(0, 2) == "00")
            {
                phone_number = phone_number.Substring(2);
            }            

            return phone_number;
        }   

    }

}
