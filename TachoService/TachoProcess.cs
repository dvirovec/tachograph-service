﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Net.Sockets;

namespace TachoService
{
    class TachoProcess
    {
        public TachoProcess(String companyName, String company_code, String port, String prefcr, String url, 
                            String username, String password, String getDDDExePath, String location, String backupLocation) {

            this.companyName = companyName;
            this.company_code = company_code;
            this.port = port;
            this.prefcr = prefcr;
            this.base_url = url;
            this.username = username;
            this.password = password;
            this.getDDDExePath = getDDDExePath;
            this.dataFilesLocation = location;
            this.backupFilesLocation = backupLocation;

            this.processFolder = Config.TACHO_SERVICE_ROOT_DIR + companyName;

            vehicleDb = new VehicleDb();
        }

        public String companyName;
        public String company_code;
        public String port;
        public String prefcr;
        public String base_url;
        public String getDDDExePath;

        public Process process;
        public String processId;
        public String processFolder;
        public String dataFilesLocation;
        public String backupFilesLocation;

        public int messageId=0;

        public String username;
        public String password;

        public WebClient client = null;

        public VehicleDb vehicleDb;

        private FileSystemWatcher watcher;
            
        
        public Boolean init_logger(String processId) { 

        if (!Config.dictLoggers.ContainsKey(processId))
                Config.dictLoggers.Add(processId, new Logger(processFolder + "\\Log", companyName));

            if (Config.dictLoggers[processId] == null)
            {
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Error, String.Format("Error creating log for {0}.", companyName));
                return false;
            }

            return true;
        }

        public Boolean init()
        {            
            try
            {
                if (!System.IO.Directory.Exists(processFolder)) {
                    System.IO.Directory.CreateDirectory(processFolder);
                }
            }
            catch {
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Error, String.Format("{0}:Process folder does not exists", companyName));
                return false;
            }

            try
            {
                if (!System.IO.Directory.Exists(processFolder + "\\Log")) {
                    System.IO.Directory.CreateDirectory(processFolder + "\\Log");
                }
            }
            catch
            {
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Error, String.Format("{0}:Log folder does not exists",companyName));
                return false;
            }

            if (!vehicleDb.init(this)) {
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Critical, String.Format("{0} :Database {1} connection failed.", companyName, Config.DB_FILE_NAME));
                return false;
            }

            Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Verbose, "VehicleDb database connection OK.");

            // File system watcher initialization

            watcher = new FileSystemWatcher();
            watcher.Path = dataFilesLocation;
            watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.Attributes |
                                    NotifyFilters.CreationTime |
                                    NotifyFilters.FileName |
                                    NotifyFilters.LastAccess |
                                    NotifyFilters.LastWrite |
                                    NotifyFilters.Size |
                                    NotifyFilters.Security;

            watcher.Filter = "*.DDD";

            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);

            watcher.EnableRaisingEvents = true;

            if(watcher!=null)
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Verbose, String.Format("File system watcher intialized for process {0}.",this.company_code));

            return true;
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {                        
               Config.getLogger(this.processId).logLine(Logger.LogLevel.Error, String.Format("File {0} download finished.", e.Name));
            
                var destFile = Path.Combine(this.backupFilesLocation, e.Name);
                var sourceFile = Path.Combine(this.dataFilesLocation, e.Name);
                

            if(File.Exists(sourceFile))
                File.Copy(sourceFile, destFile, true);

            DDDFileInfo fInfo = null;

            if (!Config.fileInfo.ContainsKey(this.processId))
            {
                fInfo = new DDDFileInfo { FileName = e.Name };
                Config.fileInfo.Add(this.processId, fInfo);
            }
            else {
                fInfo = Config.fileInfo[this.processId];
                fInfo.FileName = e.Name;
            }
           
        }

        public byte[] Post(String method, NameValueCollection data)
        {
            if (base_url == null) return null;

            byte[] response = null;

            if (client == null)
                client = new WebClient();

            if (!client.IsBusy)
            {
                string url = String.Format("{0}/{1}", base_url, method);
                try {
                    response = client.UploadValues(url, "POST", data);
                }
                catch(Exception ex) {
                    Config.getLogger(companyName).logLine(Logger.LogLevel.Error, String.Format("{0}->{1}->{2}", url, data.ToString(), ex.Message ));
                }
            }
            return response;
        }

        public byte[] Get(String method)
        {
            byte[] response = null;

            if (client == null)
                client = new WebClient();

            if (!client.IsBusy)
            {
                string url = String.Format("{0}/{1}", base_url, method);
               response = client.DownloadData(url);
            }

            return response;
        }
    }
}
