﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Data;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Net;
using System.Collections.Specialized;
using System.Threading;
using System.Web.Script.Serialization;

namespace TachoService
{
    class VehicleDb
    {
        private Thread transferThread = null;

        SqlCeConnection conn;

        TachoProcess process;

        public Boolean init( TachoProcess process) {

            this.process = process;
            
            try {
                conn = new SqlCeConnection(String.Format("Data Source={0}",  String.Format("{0}\\{1}", process.getDDDExePath, Config.DB_FILE_NAME)));
                conn.Open();

                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, String.Format("Database {0}\\{1} successfully opened.", 
                    process.getDDDExePath, Config.DB_FILE_NAME));
            }
            catch (Exception ex) {
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Critical, ex.Message);
                return false;
            }
            finally {
                conn.Close();
            }
            
            Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, Config.DB_FILE_NAME);            

            return true;

        }

        public void uploadFile(String fileName) {

            if (Config.BASE_URL == null) return;

            Uri url = new Uri(String.Format("{0}/{1}", process.base_url, "load_ddd_file"));

            string filePath = String.Format("{0}/{1}/{2}",Config.TACHO_SERVICE_ROOT_DIR, process.companyName, fileName);

            if (System.IO.File.Exists(filePath)) 
                using (var client = new WebClient())
                    client.UploadFileTaskAsync(url, "POST", filePath);
        }

        public String getIMEIforFile(String fileName) {

            DataTable t = new DataTable("IMEI");
            SqlCeDataAdapter a = new SqlCeDataAdapter(@"SELECT IMEI FROM History WHERE Files = @FileName", conn);

            String IMEI="";

            a.SelectCommand.Parameters.AddWithValue("@FileName", fileName);

            t.Clear();

            if(conn.State == ConnectionState.Closed)
                conn.Open();

            int cnt = a.Fill(t);

            if (cnt>0) {
                IMEI = t.Rows[0]["IMEI"].ToString();
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, String.Format("IMEI for file {0} found {1}.", fileName, IMEI));
            }
            
            conn.Close();

            return IMEI;
        }

        public void transferHistory() {

            string timestamp;

           List <History> historyList = new List<History>();

            DataTable t = new DataTable("History");
            SqlCeDataAdapter a = new SqlCeDataAdapter(@"SELECT Id, Timestamp, Number, Files, IMEI, 
                                 TachoTimeFrom, TachoTimeTo, TachoTimeTo, DataBlocks FROM History
                                 WHERE Timestamp > @LastTimeStamp
                                 ORDER BY Timestamp", conn);
            
            a.SelectCommand.Parameters.AddWithValue("@LastTimeStamp", new DateTime(2016, 1, 1));

            byte[] response;
           
            while (true)
            {                
                response = process.Post("timestamp", new NameValueCollection() { { "company", process.companyName } });

                if (response == null) continue;

                timestamp = System.Text.Encoding.UTF8.GetString(response);
                
                if (timestamp != null)
                {
                    DateTime dt_ts = DateTime.Parse(timestamp);
                    dt_ts = dt_ts.AddSeconds(1.0);
                    a.SelectCommand.Parameters["@LastTimeStamp"].Value = dt_ts;                    
                }

                t.Clear();

                conn.Open();
                
                int cnt = a.Fill(t);
                
                conn.Close();

                if (cnt > 0)
                {

                    MemoryStream stream1 = new MemoryStream();
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(History));

                    historyList.Clear();

                    foreach (DataRow row in t.Rows)
                    {
                        History historyRow = new History()
                        {
                            Id = row["Id"] == DBNull.Value ? new Guid() : (Guid)row["Id"],
                            Timestamp = row["Timestamp"] == DBNull.Value ? null : ((DateTime)row["Timestamp"]).ToString("yyyy-MM-dd HH:mm:ss"),
                            Number = row["Number"] == DBNull.Value ? null : (String)row["Number"],
                            Files = row["Files"] == DBNull.Value ? null : (String)row["Files"],
                            IMEI = row["IMEI"] == DBNull.Value ? null : (String)row["IMEI"],
                            TachoTimeFrom = row["TachoTimeFrom"] == DBNull.Value ? null : ((DateTime)row["TachoTimeFrom"]).ToString("yyyy-MM-dd hh:mm:ss"),
                            TachoTimeTo = (row["TachoTimeTo"] == DBNull.Value) ? null : ((DateTime)row["TachoTimeTo"]).ToString("yyyy-MM-dd hh:mm:ss"),
                            DataBlocks = row["DataBlocks"] == DBNull.Value ? null : (String)row["DataBlocks"]
                        };
                        historyList.Add(historyRow);
                    }

                    String json = new JavaScriptSerializer().Serialize(historyList);
                    
                    response = process.Post("save_records", new NameValueCollection() { { "record", json }, { "company", process.companyName } });
                   
                }
                              
                Thread.Sleep(30000);
            }
        }
        

        public void startTransfer() {

            if (Config.BASE_URL == null) return;

            transferThread= new Thread(() => transferHistory());
            transferThread.Name = "Transfer thread";
            transferThread.Start();            
        }


    }


}
