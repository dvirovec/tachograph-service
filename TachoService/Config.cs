﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace TachoService
{
    static class Config
    {        
        public const  String TACHO_SERVICE_ROOT_DIR = "C:\\Tacho\\";
        public const  String TACHO_CONFIG_FILE = "TachoServiceConfig.xml";
        public const  String TACHO_SERVICE_LOG_DIR = Config.TACHO_SERVICE_ROOT_DIR + "Log";

        public static String GET_DDD_EXE_PATH = "C:\\Teltonika\\bin\\GetDDD.exe";        

        public static String SERVICE_NAME = "TMatixTachoService";

        public static String DB_FILE_NAME = "VehicleDB.sdf";

        public static String BASE_URL = null;

        public static String API_ID = "302566";
        
        public static Int32 TMDC_PORT = -1;
        public static String TMDC_SERVER = null;

        public static String currentLogDate = null;

        public static Dictionary<string, Logger> dictLoggers = new Dictionary<string, Logger>();
        
        public static string TMATIX_IP = "54.171.31.69";
        public static int TMATIX_PORT = 30050;

        public static List<TachoProcess> processList;

        public static Dictionary<string, DDDFileInfo> fileInfo = new Dictionary<string, DDDFileInfo>();

        public static Logger getLogger(String loggerName) {
            return dictLoggers[loggerName];
        }

        public static Boolean getConfiguration() {

            XmlDocument xmlDoc = new XmlDocument();

            if (System.IO.File.Exists(TACHO_SERVICE_ROOT_DIR + TACHO_CONFIG_FILE)) {                
                try {                    
                    xmlDoc.Load((TACHO_SERVICE_ROOT_DIR + TACHO_CONFIG_FILE));
                }
                catch(Exception ex) {
                    Config.getLogger(Config.SERVICE_NAME).logFileLine(Logger.LogLevel.Error, String.Format("Error reading configuration !\n{0}\n", ex.Message));
                    return false;
                }

                Config.getLogger(Config.SERVICE_NAME).logFileLine(Logger.LogLevel.Information, String.Format("Configuration loaded."));

                if (!parseConfiguration(xmlDoc)) {
                    Config.getLogger(Config.SERVICE_NAME).logFileLine(Logger.LogLevel.Error, String.Format("Configuration parsing error."));
                    return false;
                }
            }
            return true;
        }

        private static Boolean parseConfiguration(XmlDocument xmlDoc) {
            
            XmlNode serviceNode = xmlDoc.DocumentElement.SelectNodes("/TachoService/ServiceName").Item(0);

            if (serviceNode.Name.Equals("ServiceName"))
            {
                Config.SERVICE_NAME = serviceNode.FirstChild.Value;
            }
            else {
                Config.getLogger(Config.SERVICE_NAME).logFileLine(Logger.LogLevel.Error, String.Format("Error reading configuration !\n{0}\n", "ServiceName element"));
                return false;
            }

            XmlNode tmdcConfiguration = xmlDoc.DocumentElement.SelectNodes("/TachoService/TMDCConfiguration").Item(0);

            if (tmdcConfiguration != null) {
                Config.TMDC_SERVER = tmdcConfiguration.Attributes.Item(0).Value;
                Config.TMDC_PORT = Int32.Parse(tmdcConfiguration.Attributes.Item(1).Value);
            }

            XmlNode baseRoot = xmlDoc.DocumentElement.SelectNodes("/TachoService/BaseUrl").Item(0);

            if(baseRoot!=null)
                if (baseRoot.Name.Equals("BaseUrl"))
                {
                    Config.BASE_URL = baseRoot.FirstChild.Value;
                }
                else
                {
                    Config.getLogger(Config.SERVICE_NAME).logFileLine(Logger.LogLevel.Error, String.Format("Error reading configuration !\n{0}\n", "ServiceName element"));
                    return false;
                }


            //XmlNode pathNode = xmlDoc.DocumentElement.SelectNodes("/TachoService/GetDDDexePath").Item(0);

            //if (pathNode.Name.Equals("GetDDDexePath"))
            //{
            //    Config.GET_DDD_EXE_PATH = pathNode.FirstChild.Value;
            //}
            //else {
            //    Config.getLogger(Config.SERVICE_NAME).logFileLine(Logger.LogLevel.Error, String.Format("Error reading configuration !\n{0}\n", "GetDDDexePath element"));
            //    return false;
            //}

            
            XmlNode logLevel = xmlDoc.DocumentElement.SelectNodes("/TachoService/MaxDbLogLevel").Item(0);

            //if (logLevel.Name.Equals("MaxDbLogLevel"))
            //{
            //    Config.GET_DDD_EXE_PATH = pathNode.FirstChild.Value;

            //    Config.DB_FILE_NAME = Config.GET_DDD_EXE_PATH.Substring(0,(Config.GET_DDD_EXE_PATH.LastIndexOf("\\")+1)) + Config.DB_FILE_NAME;
            //}
            //else
            //{
            //    Config.getLogger(Config.SERVICE_NAME).logFileLine(Logger.LogLevel.Error, String.Format("Error reading configuration !\n{0}\n", "MaxDbLogLevel element"));
            //    return false;
            //}

            String companyName;
            String company_code;
            String url;
            String prefcr;
            String port;
            String username="";
            String password="";
            String dataFilesLocation;
            String backupFilesLocation;
            String getDDDExePath;
            
            XmlNodeList nodes = xmlDoc.DocumentElement.SelectNodes("/TachoService/ProcessList/Process");

            if (nodes.Count > 0) {
                Config.processList = new List<TachoProcess>();

                foreach (XmlNode node in nodes) {

                    companyName = node.Attributes["CompanyName"].Value;
                    company_code = node.Attributes["company_code"].Value;

                    port = node.Attributes["port"].Value;
                    prefcr = node.Attributes["prefcr"].Value;
                    dataFilesLocation = node.Attributes["DataFilesLocation"].Value;

                    backupFilesLocation = node.Attributes["BackupFilesLocation"].Value;
                    getDDDExePath = node.Attributes["GetDDDexePath"].Value;
                    
                    if (node.Attributes["username"]!=null)
                    {
                        username = node.Attributes["username"].Value;
                        password = node.Attributes["password"].Value;
                    }

                    if (node.Attributes["Url"] != null)
                        url = node.Attributes["Url"].Value;
                    else
                        url = Config.BASE_URL;
                                        
                    Config.processList.Add(new TachoProcess(companyName, company_code, port, prefcr, url, username, password, getDDDExePath, dataFilesLocation, backupFilesLocation));
                }
            }
            else {
                Config.getLogger(Config.SERVICE_NAME).logFileLine(Logger.LogLevel.Error, String.Format("Error reading configuration !\n{0}\n", "Processes not defined."));
                return false;
            }

            return true;
        }

        public static TachoProcess getProcessById(String processId) { 

         foreach (TachoProcess proc in Config.processList)
            {
                if (proc.processId == processId)
                    return proc;
            }
            return null;
        }

    }
}
