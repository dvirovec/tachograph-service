﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TachoService
{
    class TMDCComm
    {        

        public Boolean sendMesage(string IMEI, string timestamp, string message, string pid) {

            string message_time = DateTime.UtcNow.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss");

            string message2Send = @"{"""+IMEI+@"_TACHO"":{""version"":""0.1"",""time"":"""+message_time+@""",""raw_text"":""SMS received"",""is_error"":false,""tcu_time"":"""+timestamp+@""",""text"":"""+message+@"""}}";
            
            Boolean messageSent = false;

            TcpClient client = null;

            try
            {
                client = new TcpClient(Config.TMDC_SERVER, Config.TMDC_PORT);

                Byte[] data = System.Text.Encoding.ASCII.GetBytes(message2Send);

                using (NetworkStream stream = client.GetStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                messageSent = true;
            }
            catch (Exception ex)
            {                
                Config.getLogger(pid).logLine(Logger.LogLevel.Error, String.Format("TMDC Communication error. {0}",ex.Message));
            }
            finally
            {
                if (client != null) client.Close();
            }

            return messageSent;
        }
    }
}
