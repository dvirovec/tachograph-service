﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Security;
using System.Net.Sockets;
using System.ServiceModel;
using System.IO;
//using System.ServiceModel;

namespace TachoService
{

    public partial class TachoService : ServiceBase
    {              
        private Boolean killedByService = false;

        public ServiceHost serviceHost = null;

        public TachoService()
        {
            InitializeComponent();
        }

        public void onDebug() {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            Config.dictLoggers.Add(Config.SERVICE_NAME, new Logger(Config.TACHO_SERVICE_LOG_DIR, Config.SERVICE_NAME));

            if (!Config.getConfiguration())
            {
                this.Stop();
                return;
            }

            if (Config.getLogger(Config.SERVICE_NAME) == null)
            {
                this.Stop();
                return;
            }

            if (!checkFolders()) {
                this.Stop();
                return;
            };            
         
            Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, "Service starting.");

            foreach(TachoProcess proc in Config.processList) {                
                startProcess(proc, String.Format("{0}GetDDD.exe", proc.getDDDExePath), proc.getDDDExePath);
            }

            try
            {
                if (serviceHost != null)
                {
                    serviceHost.Close();
                }

                serviceHost = new ServiceHost(typeof(SmsErrorHandling));

                serviceHost.Open();

                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Critical, "WCF service host opened sucesfully.");

            }
            catch (Exception e)
            {
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Error, "WCF service host error.Error opening host:" + e.Message);
            }
            
            Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information,"Service started.");
        }

        protected override void OnStop()
        {
            Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information,"Service stopping.");

            String companyName="";

            if (Config.processList != null)
                if (Config.processList.Count > 0)
                    foreach (TachoProcess proc in Config.processList) {
                        try {
                            companyName = proc.companyName;
                            killedByService = true;
                            Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, String.Format("Process for {0} stopping by service ...", companyName));
                            proc.process.Kill();
                            Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, String.Format("Process for {0} stopped by service.", companyName));
                        }
                        catch (Exception ex)
                        {
                            Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, String.Format("Stopping process for {0} failed.\n{1}", companyName, ex.Message));
                            return;
                        }
                    }
            try
            {
                if (serviceHost != null)
                {
                    serviceHost.Close();
                    serviceHost = null;
                }
            }
            catch (Exception e)
            {
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Error, "WCF service host error.Error closing host: " + e.Message);
            }

            Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, "Service stopped.");
        }

        private void startProcess(TachoProcess tachoProcess, String fileName, String workingDirectory) {
            

            if (String.IsNullOrEmpty(tachoProcess.dataFilesLocation))
                tachoProcess.dataFilesLocation = Config.TACHO_SERVICE_ROOT_DIR + tachoProcess.companyName;
            
            if (!tachoProcess.init())
            {
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Error, String.Format("Problem while processing configuration {0}.", tachoProcess.companyName));
                return;
            }
            else
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Verbose, String.Format("Process configuration OK {0}.", tachoProcess.dataFilesLocation));

            Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, String.Format("Process for {0} starting.", tachoProcess.companyName));

            try
            {
                String arguments = String.Format("start --port={0} --prefcr=\"{1}\" --dataDir={2}", tachoProcess.port, tachoProcess.prefcr, tachoProcess.dataFilesLocation);                
                
                tachoProcess.process = new Process()
                {
                    EnableRaisingEvents = true,

                    StartInfo = new ProcessStartInfo() {
                          
                            FileName = fileName,                    
                            
                            CreateNoWindow = true,
                            UseShellExecute = false,                            
                            WindowStyle = ProcessWindowStyle.Hidden,

                            Arguments = arguments,
                            
                            WorkingDirectory = tachoProcess.processFolder,

                            RedirectStandardOutput = true                                                        
                    }
                };



                if (!String.IsNullOrEmpty(tachoProcess.username))
                {
                    SecureString securePwd = new SecureString();
                    
                    foreach (char c in tachoProcess.password)
                    {
                        securePwd.AppendChar(c);
                    }

                    securePwd.MakeReadOnly();

                    tachoProcess.process.StartInfo.Verb = "runas";
                    tachoProcess.process.StartInfo.UserName = tachoProcess.username;
                    tachoProcess.process.StartInfo.Password = securePwd;
                    tachoProcess.process.StartInfo.Domain = ".";                    

                }

                tachoProcess.process.Exited += new EventHandler(TachoProcess_Exited);

                tachoProcess.process.OutputDataReceived += new DataReceivedEventHandler((sender, e) => {
                
                Boolean fileReceived = false;

                String messageToBeSend = "";
                String content = (String)e.Data;

                if (!String.IsNullOrEmpty(e.Data))
                {

                    //Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Verbose, content);

                    fileReceived = content.Contains("Downloading to Downloaded");

                    if (content.Contains("Downloading to Downloaded") ||
                        content.Contains("ErrorIgnitionOff") ||
                        content.Contains("Authenticated to Downloading")) {
                            
                        if (content.Contains("Authenticated to Downloading"))
                            messageToBeSend = "Authenticated to file download start.";

                        if (content.Contains("Downloading to Downloaded"))
                            messageToBeSend = "File {0} downloading ...";

                        if (content.Contains("ErrorIgnitionOff"))
                            messageToBeSend = "Error ignition off - file download stopped.";

                        
                        String timestamp = content.Substring(0, 15);

                        timestamp = timestamp.Replace(" ", "") + " " + content.Substring(15, 8); ;

                        int pos = content.IndexOf("IMEI:") + 6;
                        String IMEI = content.Substring(pos, 15);

                            string pid = ((System.Diagnostics.Process)sender).Id.ToString();

                            if (fileReceived)
                            {
                                DDDFileInfo fInfo = null;

                                if (Config.fileInfo.ContainsKey(pid))
                                {

                                    fInfo = Config.fileInfo[pid];

                                    fInfo.IMEI = IMEI;
                                    fInfo.Timestamp = timestamp;
                                    fInfo.Message = messageToBeSend;
                                    fInfo.DownloadFinished = true;
                                }
                                else
                                {
                                    fInfo = new DDDFileInfo { IMEI = IMEI, Timestamp = timestamp, Message = messageToBeSend, DownloadFinished = true };
                                    Config.fileInfo.Add(pid, fInfo);
                                }

                                fInfo.Message = String.Format(fInfo.Message,fInfo.FileName);

                                TMDCComm comm = new TMDCComm();
                                comm.sendMesage(fInfo.IMEI, fInfo.Timestamp, fInfo.Message, pid);                           
                                Config.getLogger(pid).logLine(Logger.LogLevel.Information, String.Format("IMEI:{0} Message:{1}", fInfo.IMEI, fInfo.Message));
                            }

                            if (!fileReceived)
                            Config.getLogger(pid).logLine(Logger.LogLevel.Information, 
                                        String.Format("IMEI:{0} Message:{1}", IMEI, messageToBeSend));
                                
                        }
                                                                
                        }                            
                        
                    });

                    if (tachoProcess.process.Start())
                    {

                    tachoProcess.init_logger(tachoProcess.process.Id.ToString());

                    if (Config.dictLoggers[tachoProcess.process.Id.ToString()] == null)
                    {
                        Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Error, String.Format("{0} : Error creating process logger.", tachoProcess.companyName));
                        return;
                    }

                    Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information,
                            String.Format("Process for {0} starting with arguments; {1}.", tachoProcess.companyName, arguments));

                    tachoProcess.process.BeginOutputReadLine();
                        
                        if(Config.BASE_URL!=null)
                            tachoProcess.vehicleDb.startTransfer();

                    Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, String.Format("Process for {0} started with arguments; {1}.", tachoProcess.companyName, tachoProcess.process.StartInfo.Arguments));

                    tachoProcess.processId = tachoProcess.process.Id.ToString();

                    }
                    else {
                        Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, String.Format("Process for {0} failed to start {1}.\n", tachoProcess.companyName, fileName));
                    }

            }
            catch (Exception ex)  {
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, String.Format("Process for {0} failed to start {1}.\n{2}", tachoProcess.companyName, fileName, ex.Message));
                return;
            }



            Config.getLogger(tachoProcess.process.Id.ToString()).setTachoProcess(tachoProcess);            
        }

        void OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            String line = e.Data;
        }

        private void TachoProcess_Exited(object sender, System.EventArgs e)
        {
            if (killedByService)
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Information, String.Format("Process for {0} killed by service.",  Config.getProcessById(((Process)sender).Id.ToString()).companyName));
            else
            {
                Config.getLogger(Config.SERVICE_NAME).logLine(Logger.LogLevel.Critical, String.Format("Process for {0} stoped unexpectedly. Trying restart ...", Config.getProcessById(((Process)sender).Id.ToString()).companyName));
                tryProcessStart(((Process)sender).Id.ToString());
            }
        }

        private void tryProcessStart(String processId) {

            foreach (TachoProcess proc in Config.processList)
            {
                if(proc.processId == processId)                    
                    startProcess(proc, String.Format("{0}GetDDD.exe", proc.getDDDExePath), proc.getDDDExePath);
            }
        }

        private Boolean checkFolders() {

            try {
                if (!System.IO.Directory.Exists(Config.TACHO_SERVICE_ROOT_DIR)) {
                    System.IO.Directory.CreateDirectory(Config.TACHO_SERVICE_ROOT_DIR);                    
                }
            }
            catch {
                return false;
            }
            return true;
        }

    }
}
