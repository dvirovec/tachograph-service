﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TachoService
{

    class Logger
    {

        public enum LogLevel {           
            All = 0,
            Critical=1,  
            Error=2,    
            Warning=3,
            Information=4,
            Verbose=5                  
        }

        private TachoProcess process;

        private String logFileName = null;
        private System.IO.StreamWriter fileStream = null;
        private String logFilePath, logName;


        public Logger(String logFilePath, String logName ) {

            if (!System.IO.Directory.Exists(logFilePath)) {
                System.IO.Directory.CreateDirectory(logFilePath);                
            }

            this.logFilePath = logFilePath;
            this.logName = logName;

            logFileForDate();
        }
        
        public void logFileForDate() {

            if ((Config.currentLogDate == null) || !Config.currentLogDate.Equals(DateTime.Now.ToString("dd.MM.yyyy")))
                Config.currentLogDate = DateTime.Now.ToString("dd.MM.yyyy");            
            
            logFileName = createLogFullFileName(this.logFilePath, this.logName, Config.currentLogDate);

            if (!System.IO.File.Exists(logFileName)) {
                System.IO.FileStream fs = System.IO.File.Create(logFileName);
                fs.Close();
                logLine(Logger.LogLevel.Verbose, String.Format("Log file {0} created.", logFileName));
            }           
        }

        public void logFileLine(LogLevel level, String logLine)
        {
            try
            {
                fileStream = new System.IO.StreamWriter(logFileName, true);
                fileStream.WriteLine(String.Format("{0} {1}", DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"), logLine));
                fileStream.Close();
            }
            catch (Exception ex)
            {
                Config.getLogger(Config.SERVICE_NAME).logFileLine(Logger.LogLevel.Error, String.Format("Log transfer {0}",ex.Message)); 
            }
        }

        public void logLine(LogLevel level, String logLine) {

            logFileForDate();

            logFileLine(level, logLine);

            string company_code;

            string logLevel = ((int)level).ToString();

            if (process == null) {
                company_code = "service";
            }
            else {
                company_code = this.process.company_code;
            }

            if (Config.BASE_URL != null) {                

                Post("log_line", new NameValueCollection() { { "company_code", company_code }, { "logLevel", logLevel }, { "logText", logLine } });
            }
        }

        public void Post(String method, NameValueCollection data)
        {
            if (Config.BASE_URL != null)
            {
                Uri url = new Uri(String.Format("{0}/{1}", process != null ? process.base_url : Config.BASE_URL, method));

                using (var client = new WebClient())
                    client.UploadValuesAsync(url, "POST", data);
            }
        }


        private String createLogFullFileName(String logFilePath, String logName, String logDate)
        {
            String logFileName = String.Format("{0}\\{1}_{2}.log", logFilePath, logName, logDate);
            return logFileName;
        }

        public void setTachoProcess(TachoProcess process) {
            this.process = process;
        }

        public TachoProcess getTachoProcess()
        {
            return this.process;
        }

    }
}
