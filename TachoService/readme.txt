﻿1. Installation
Create folder C:/Tacho
Start CMD as Administrator
Copy TachoServiceConfig.xml to C:/Tacho folder
Change configuration if needed
Go to Release folder or to folder where TachoService.exe file exists 
Execute "C:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe" "TachoService.exe" from command line

If uninstall of service is necessary use option /u like this "C:\Windows\Microsoft.NET\Framework\v4.0.30319\InstallUtil.exe" /u "TachoService.exe"

2. Starting
Open services 
Find and select T-Matics tachogeph service
Mouse right-click select Start and click
Wait for service start complete

3. Check results
Goto C:/Tacho/Log
Open last TMatixTachoService_... log file.
