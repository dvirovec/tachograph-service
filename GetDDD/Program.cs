﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace GetDDD
{
    class Program
    {
        
        private static String url = "https://www.hnb.hr/hnb-tecajna-lista-portlet/rest/tecajn/getformatedrecords.dat";

        private static String workingDirectory= "C:\\Tacho\\Data\\Strabag\\";
        private static String port = "0000";
        private static int fileCount = 0;


        static void Main(string[] args)
        {
            if (args.Length == 0) {
                args = new string[] { "--dataDir=C:\\Tacho\\Data\\Strabag\\" };
            }

            foreach (string s in args)
            {
                if (s.Contains("--dataDir"))
                {
                    Program.workingDirectory = s.Split('=')[1];
                }

                if (s.Contains("--port"))
                {
                    Program.port = s.Split('=')[1];
                }

                

            }
            while (true) {

                TimerEventProcessor();
                var watch = System.Diagnostics.Stopwatch.StartNew();
                while (watch.ElapsedMilliseconds < 10000)
                {

                }
            }
            
        }

        private static void TimerEventProcessor()
        {            

            Console.Out.WriteLine("-***********- Card Driver Activity -***********-");

            Console.Out.WriteLine("Downloading data: {0}", DateTime.Now.ToString("dd.MM.yyyy h:mm:ss"));

            ExchangeList exl = new ExchangeList(url, "GET");

            String response = exl.GetResponse();

            //System.IO.StreamWriter fs = new System.IO.StreamWriter("M_" + DateTime.Now.ToString("yyyyMMdd_hhmm")+ "_G 908MF_WMA84SZZ1DL067704.DDD", true);
            System.IO.StreamWriter fs = new System.IO.StreamWriter(Program.workingDirectory + port+"_"+fileCount+"_M_20160420_1538_G 908MF_WMA84SZZ1DL067704.DDD", true);
            fileCount++;


            fs.Write(response);

            var watch = System.Diagnostics.Stopwatch.StartNew();
            while(watch.ElapsedMilliseconds<5000) {
                
            }

            fs.Write("*****************************************************\n");

            fs.Close();
                       
            Console.Out.WriteLine(String.Format("File {0} created.", "Tacho_" + DateTime.Now.ToString("dd_MM_yyyy hh_mm_ss") + ".ddd"));

            Console.Out.WriteLine("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
            Console.Out.WriteLine("2016 - 11 - 29 10:05:22,959 INFO Teltonika.Ddd.Common.VehicleUnit TcpDataReceiver #2 - Changed VehicleUnit [] (IMEI: 356173065953650) state from Authenticated to Downloading.");
            Console.Out.WriteLine("2016 - 10 - 05 14:20:42,877 INFO Teltonika.Ddd.Common.VehicleUnit TcpDataReceiver #0 - Changed VehicleUnit [] (IMEI: 356173065953650) state from ErrorIgnitionOff.");
            Console.Out.WriteLine("2016 - 10 - 05 14:20:42,877 INFO Teltonika.Ddd.Common.VehicleUnit TcpDataReceiver #0 - Changed VehicleUnit [] (IMEI: 356173065953650) state from Downloading to Downloaded.");
            Console.Out.WriteLine("2016 - 11 - 22 15:35:44,695 DEBUG Teltonika.Ddd.Common.SmartCardManager 59 - Release(imei: 357454070927528)");

            Thread.Sleep(5000);
            
        }
        

    }
}
